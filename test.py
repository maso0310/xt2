import urllib3
import os
#PIL影象處理標準庫
from PIL import Image
from io import BytesIO
http = urllib3.PoolManager()
response = http.request('GET','f.hiphotos.baidu.com/image/pic/item/8d5494eef01f3a29f863534d9725bc315d607c8e.jpg')
result = response.data
#將bytes結果轉化為位元組流
bytes_stream = BytesIO(result)
#讀取到圖片
print(bytes_stream)

import subprocess
import io
exiftool_path = 'exiftool-12.35.exe'
exiftool_path = os.path.abspath(exiftool_path)
filename = 'DJI_0254_R.JPG'
thermal_img_bytes = subprocess.check_output([exiftool_path, "-rawthermalimage", "-b", filename])
thermal_img_stream = io.BytesIO(thermal_img_bytes)
roiimg = Image.open(thermal_img_stream)

#roiimg = Image.open(bytes_stream)
# roiimg.show() #展示圖片
#print(type(result))
#print(response.status)
imgByteArr = BytesIO() #初始化一個空位元組流
roiimg.save(imgByteArr,format('PNG'))  #把我們得圖片以‘PNG'儲存到空位元組流
imgByteArr = imgByteArr.getvalue() #無視指標，獲取全部內容，型別由io流變成bytes。
# dir_name = os.mkdir('baiduimg')
img_name = '1.jpg'
with open(os.path.join('baiduimg',img_name),'wb') as f:
 f.write(imgByteArr)