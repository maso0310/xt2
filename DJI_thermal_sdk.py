import os 
import glob 
from pathlib import Path


for image_path in glob.glob('./example_input/*.jpg'):
    #print(image_path)
    exe_path = r'DJI_thermal_SDK_v1.2\\utility\\bin\\windows\\release_x64\\dji_irp.exe'
    os.system(f'{exe_path} -s {image_path} -a measure -o \\example_output\\measure_float.raw --measurefmt float32')
    break