#用這個轉換sRGB跟temperarture數值
import os
from pathlib import Path
import glob 
import subprocess


fomat = 'jpg'
date = '0410'

for image_path in glob.glob(f'./input/{date}/*.jpg'):
    print(image_path)
    exiftool_path = 'exiftool-12.35.exe'
    exiftool_path = os.path.abspath(exiftool_path)

    image_name = Path(image_path).stem
    output_dir = f'./output/{fomat}/{date}'
    if os.path.isdir(output_dir)==False:
        os.mkdir(output_dir)
    output_path = f'./output/{fomat}/{date}/{image_name}.{fomat}'

    #thermal_img_bytes = subprocess.check_output([exiftool_path, "-rawthermalimage", "-b", filename])
    os.system(f'{exiftool_path} {image_path} -rawthermalimage -b > {output_path}')
