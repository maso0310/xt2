import numpy as np
from thermal import Thermal

thermal = Thermal(
    dirp_filename='plugins/dji_thermal_sdk_v1.1_20211029/windows/release_x64/libdirp.dll',
    dirp_sub_filename='plugins/dji_thermal_sdk_v1.1_20211029/windows/release_x64/libv_dirp.dll',
    iirp_filename='plugins/dji_thermal_sdk_v1.1_20211029/windows/release_x64/libv_iirp.dll',
    exif_filename='plugins/exiftool-12.35.exe',
    dtype=np.float32,
)


import glob
from pathlib import Path
from cv2 import cv2 as cv 

for image_path in glob.glob('./input/*.jpg'):
    image_path = 'DJI_XT2.jpg'
    print(image_path)
    img = cv.imread(image_path)
    print(img.shape)
    temperature = thermal.parse_dirp2(image_filename=image_path)
    print(type(temperature))
    print(temperature)
    print(temperature.shape)
    assert isinstance(temperature, np.ndarray)
    break